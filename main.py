import time
import pyautogui 
import sys

print('Starting PyAutoMouse...')
pyautogui.FAILSAFE = True

# Declarations
c11 = (430, 313)
c22 = (1396, 313)
c33 = (521, 701)
c44 = (1492, 700)
nextPos = (1224, 1034)
mouse_delay = 1
loops = 30
loopDelay = 4

print('* Window size is ' + str(pyautogui.size()))
print('* Looping for ' + str(loops))
print('PRESS CTRL+C TO QUIT')

try:
    for i in range(loops):
        print('Executing sequence ' + str(i+1) + '/' + str(loops))
        
        print('C1: moving mouse to x:' + str(c11[0]) + ', y:' + str(c11[1]))
        pyautogui.moveTo(c11[0], c11[1], mouse_delay)
        pyautogui.click()

        print('C2: moving mouse to x:' + str(c22[0]) + ', y:' + str(c22[1]))
        pyautogui.moveTo(c22[0], c22[1], mouse_delay)
        pyautogui.click()

        print('C3: moving mouse to x:' + str(c33[0]) + ', y:' + str(c33[1]))
        pyautogui.moveTo(c33[0], c33[1], mouse_delay)
        pyautogui.click()

        print('C4: moving mouse to x:' + str(c44[0]) + ', y:' + str(c44[1]))
        pyautogui.moveTo(c44[0], c44[1], mouse_delay)
        pyautogui.click()

        print('Going to next round: moving mouse to x:' + str(nextPos[0]) + ', y:' + str(nextPos[1]))
        pyautogui.moveTo(nextPos[0], nextPos[1], mouse_delay)
        pyautogui.click()

        print('Sequence finished! Sleeping for ' + str(loopDelay) + ' seconds...')
        time.sleep(loopDelay)
        i += 1
except KeyboardInterrupt:
    print('Shutting down PyAutoMouse...')
    sys.exit()

print('Total execution finished! Shutting down PyAutoMouse...')